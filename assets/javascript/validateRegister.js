function validateRegister() {
    // variable fields
    var givenName = document.getElementById('txtGivenName').value;
    var surname = document.getElementById('txtSurname').value;
    var email = document.getElementById('txtEmail').value;
    var password = document.getElementById('txtPassword').value;
    var confPassword = document.getElementById('txtConfPassword').value;

    // Error message field
    var errorMessage = document.getElementById('errMessage');

    // Email regex
    var emailRe = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Does every field have something.
    if(givenName != '' &&
       surname != '' &&
       email != '' &&
       password != '' &&
       confPassword != '') {
	// Do the passwords match
	if (password == confPassword) {
	    if (emailRe.test(email)) {
		return true;
	    } else {
		errorMessage.innerHTML = "<br />";
		errorMessage.innerHTML += "Please enter a valid email";
	    }
	} else {
	    errorMessage.innerHTML = "<br />";
	    errorMessage.innerHTML += "Passwords do not match";
	    return false;
	} 
    } else {
	errorMessage.innerHTML = "<br />";
	errorMessage.innerHTML += "All fields must be entered";
	return false;
    }
};
