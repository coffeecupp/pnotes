CREATE TABLE IF NOT EXISTS Users(
       userEmail varchar(255) NOT NULL UNIQUE PRIMARY KEY,
       givenName varchar(30) NOT NULL,
       surname varchar(45) NOT NULL,
       salty varchar(40) NOT NULL,
       userPassword varchar(255) NOT NULL);


CREATE TABLE IF NOT EXISTS Notes(
       fkUserEmail varchar(255) NOT NULL,
       noteID INT UNIQUE AUTO_INCREMENT PRIMARY KEY,
       title VARCHAR(40) NOT NULL,
       post TEXT NOT NULL,
       FOREIGN KEY (fkUserEmail) REFERENCES Users(userEmail));
