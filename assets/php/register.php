<?php
// DB Stuff
include('./dbconnect.php');
// Session handler
include('./sessionHandler.php');

// Variables from post
$userGivenName = strtolower($_POST['userGivenName']);
$userSurname = strtolower($_POST['userSurname']);
$userEmail = strtolower($_POST['userEmail']);
$userPassword = $_POST['userPassword'];

// time in seconds.
$SALT = "AceWhitebeardPiratesSecondCommander";
// Hash the password.
$hashed = hash('sha256', $userPassword . $SALT);

$SQL = <<<EOT
INSERT INTO Users(userEmail, givenName, surname, saltyTime, userPassword) 
VALUES('$userEmail', '$userGivenName', '$userSurname', '$SALT', '$hashed');
EOT;

if($conn->query($SQL)) {
    createSession($userEmail);
} else {
    echo $conn->error;
}


?>
