<?php
function createSession($userEmail) {
    if(!isset($_SESSION['email'])){
	session_start();
	$_SESSION['email'] = $userEmail;
	header("location: /assets/php/loginForm.php");
    } else {
	header("location: /assets/php/loginForm.php");
    }
}

function destroySession($userEmail) {
    if(!isset($_SESSION['email'])){
	session_destroy();
    } else {
	header("location: /assets/php/loginForm.php");
    }
}
?>
