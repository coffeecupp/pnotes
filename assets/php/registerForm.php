<html>
  <head>
    <title>pNotes Register</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/formInputs.css" />
    <script src="/assets/javascript/validateRegister.js"></script>
  </head>
  <body>
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/templates/navbar.html'); ?>
    <form class="middle" id="frmRegister" method="POST" onsubmit="return validateRegister();" action="register.php">
      <table class="middle">
	<tr>
	  <td>
	    <input type="text" id="txtGivenName" name="userGivenName" placeholder="Given Name" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="text" id="txtSurname" name="userSurname" placeholder="Surname" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="email" id="txtEmail" name="userEmail" placeholder="Email" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="password" id="txtPassword" name="userPassword" placeholder="Password" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="password" id="txtConfPassword" name="userConfirmPassword" placeholder="Confirm Password" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <p id="errMessage" class="errorMessage"></p>
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="submit" id="btnSubmit" value="Sign Up"/>
	  </td>
	</tr>
      </table>
    </form>
  </body>
</html>
