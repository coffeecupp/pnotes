<html>
  <head>
    <title>pNotes Login</title>
    <link rel="stylesheet" type="text/css" href="/assets/css/formInputs.css" />
    <script src="/assets/javascript/validateLogin.js"></script>
  </head>
  <body>
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/templates/navbar.html'); ?>
    <form class="middle" id="frmLogin" method="POST" onsubmit="validateLogin(); return false;" action="login.php">
      <table class="middle">
	<tr>
	  <td>
	    <input type="email" id="txtEmail" name="userEmail" placeholder="Email" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="password" id="txtPassword" name="userPassword" placeholder="Password" />
	  </td>
	</tr>
	<tr>
	  <td>
	    <input type="submit" id="btnSubmit" value="Login"/>
	  </td>
	</tr>
	<tr>
	  <td>
	    <p><a href="/assets/php/registerForm.php">Need an Account?</a></p>
	  </td>
	</tr
      </table>
    </form>
  </body>
</html>
