<?php
// Get the inii file
$conf = parse_ini_file($_SERVER['DOCUMENT_ROOT'] . '/config.ini');

// All variables from that ini file
$conn = new mysqli($conf['host'], $conf['user'],
		   $conf['password'], $conf['database']);

// If therer is an error dispaly it
if($conn->connect_errno > 0) {
    die('Unable to connect' . $conn->connect_error); 
}
// Otherwise do nothing.

?>
